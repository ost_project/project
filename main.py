#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import cgi
from google.appengine.api import users
from google.appengine.ext import ndb
import webapp2
import os
import urllib
from datetime import datetime
import jinja2
import time
import datetime
import uuid


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

def resource_key(current_time):
    return ndb.Key('Resource_Table', current_time)

class Resource_Table(ndb.Model):
    resource_id = ndb.StringProperty(indexed = True)
    resource_name = ndb.StringProperty(indexed = True)
    username = ndb.StringProperty(indexed = True)
    start_time = ndb.DateTimeProperty(indexed = False)
    end_time = ndb.DateTimeProperty(indexed = False)
    tags = ndb.StringProperty(indexed = False, repeated = True)
    last_modified = ndb.DateTimeProperty(indexed = True)


def reservation_key(current_time):
    return ndb.Key('Resource_Table', current_time)

class Reservation_Table(ndb.Model):
    reservation_id = ndb.StringProperty(indexed = True)
    resource_id = ndb.StringProperty(indexed = True)
    username = ndb.StringProperty(indexed = True)
    start_time = ndb.DateTimeProperty(indexed = False)
    end_time = ndb.DateTimeProperty(indexed = False)
    resource_name = ndb.StringProperty(indexed = True)

def compare_start_time(t1, t2):
    if t1.start_time > t2.start_time:
        return -1
    if t1.start_time == t2.start_time:
        return 0
    if t1.start_time < t2.start_time:
        return 1

def compare_modified_time(t1, t2):
    if t1.last_modified > t2.last_modified:
        return -1
    if t1.last_modified == t2.last_modified:
        return 0
    if t1.last_modified < t2.last_modified:
        return 1

class MainPage(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user is None:
          self.redirect(users.create_login_url(self.request.uri))
        else:
          url = users.create_logout_url(self.request.uri)
          url_linktext = 'Logout'

          q = Resource_Table.gql("")
          total_resources = list(q)
          total_resources.sort(compare_modified_time)
          
          q = Resource_Table.gql("WHERE username = :1",user.nickname())
          user_resources = list(q)
          q = Reservation_Table.gql("WHERE username = :1",user.nickname())
          user_reservations = list(q)
          user_reservations.sort(compare_start_time)
          current_time = datetime.datetime.now()
          time_diff = datetime.datetime.strptime('05:00', '%H:%M')
          delta = datetime.timedelta(hours = 5, minutes = 0)
          current_time = current_time - delta
          template_values = {
              'user_resources' : user_resources,
              'total_resources' : total_resources,
              'user_reservations' : user_reservations,
              'current_time': current_time,
              'url': url,
              'url_linktext': url_linktext,

          }
          template = JINJA_ENVIRONMENT.get_template('index.html')
          self.response.write(template.render(template_values))

class Resource(webapp2.RequestHandler):
    def get(self):
        template_values = dict()

        template = JINJA_ENVIRONMENT.get_template('create_resource.html')
        self.response.write(template.render(template_values))

    def post(self):
        isError = 0
        resource_name = self.request.get('resource_name')
        start_time = self.request.get('resource_start_time')
        end_time = self.request.get('resource_end_time')
        tags = self.request.get('tags')

        if len(resource_name) == 0:
          isError = 1
          template_values = {
              'error': 'Resource Name cannot be empty',
          }
          template = JINJA_ENVIRONMENT.get_template('create_resource.html')
          self.response.write(template.render(template_values))
          return
        elif start_time > end_time:
          template_values = {
              'error': 'Start time should be before end time',
            }
          template = JINJA_ENVIRONMENT.get_template('create_resource.html')
          self.response.write(template.render(template_values))
          return

        elif isError == 0:
          try:
            time.strptime(start_time, '%H:%M')
          except ValueError:
            isError = 1
            template_values = {
              'error': 'Start time format is incorrect and/or hours, minutes values incorrect ',
            }
            template = JINJA_ENVIRONMENT.get_template('create_resource.html')
            self.response.write(template.render(template_values))
            return

        try:
          time.strptime(end_time, '%H:%M')
          template_values = dict()
          user = users.get_current_user()
          
          current_time = time.strftime("%H:%M:%S")
          resource = Resource_Table(parent=resource_key(current_time))
          resource.resource_id = str(uuid.uuid4())
          resource.resource_name = resource_name
          resource.username = user.nickname()
          resource.start_time = datetime.datetime.strptime(start_time, '%H:%M')
          resource.end_time = datetime.datetime.strptime(end_time, '%H:%M')
          resource.tags = tags.strip().split(',')
          resource.last_modified = datetime.datetime.now()      
          resource.put()

          self.redirect("/")

        except ValueError:
          isError = 1
          template_values = {
            'error': 'End time format is incorrect and/or hours, minutes values incorrect ',
          }
          template = JINJA_ENVIRONMENT.get_template('create_resource.html')
          self.response.write(template.render(template_values))

class ViewResource(webapp2.RequestHandler):
  def get(self):
        user = users.get_current_user()
        nickname = user.nickname()
        resource_id = self.request.GET['resource']
        resource = list(Resource_Table.gql("WHERE resource_id = :1",resource_id))
        tag_string = ""
        tags = list(resource[0].tags)
        for tag in tags:
          tag_string = tag_string + tag + ","

        current_time = datetime.datetime.now()
        time_diff = datetime.datetime.strptime('05:00', '%H:%M')
        delta = datetime.timedelta(hours = 5, minutes = 0)
        current_time = current_time - delta

        reservations_of_resource = list(Reservation_Table.gql("WHERE resource_name = :1",resource[0].resource_name))
        template_values = {
          'resource': resource,
          'reservations_of_resource': reservations_of_resource,
          'nickname': nickname,
          'current_time': current_time,
          'tag_string': tag_string
        }      
        template = JINJA_ENVIRONMENT.get_template('display_resource.html')
        self.response.write(template.render(template_values))


class MakeReservation(webapp2.RequestHandler):
  def post(self):
        resource_id = str(self.request.get('resource_id'))
        template_values = {
          'resource_id': resource_id
        }      
        template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
        self.response.write(template.render(template_values))

class Reservation(webapp2.RequestHandler):
  def post(self):
        resource_id = str(self.request.get('resource_id'))
        start_time = self.request.get('reservation_start_time')
        duration = self.request.get('duration')
        date = self.request.get('reservation_date')
        isError = 0

        try:
          time.strptime(date, '%m/%d/%Y')
        except ValueError:
          template_values = {
            'resource_id': resource_id,
            'error': 'Date format is incorrect'
          }
          template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
          self.response.write(template.render(template_values))
          return

        try:
          time.strptime(start_time, '%H:%M')
        except ValueError:
          isError = 1
          template_values = {
            'resource_id': resource_id,
            'error': 'Start time format is incorrect and/or hours, minutes values incorrect ',
          }
          template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
          self.response.write(template.render(template_values))
          return

        try:
          time.strptime(duration, '%H:%M')
        except ValueError:
          isError = 1
          template_values = {
            'resource_id': resource_id,
            'error': 'Duration format is incorrect and/or hours, minutes values incorrect '
          }
          template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
          self.response.write(template.render(template_values))
          return

        resource = list(Resource_Table.gql("WHERE resource_id = :1",resource_id))        
        resource_name = resource[0].resource_name
        start_time_resource = resource[0].start_time
        end_time_resource = resource[0].end_time
        
        start_time_reservation = datetime.datetime.strptime(start_time, '%H:%M')
        duration = datetime.datetime.strptime(duration, '%H:%M')

        dt2 = datetime.timedelta(hours=duration.hour, minutes=duration.minute)

        
        total = start_time_reservation + dt2
        end_time_reservation = total
        end_time_string = end_time_reservation.strftime('%H:%M')

        if (start_time_resource > start_time_reservation) or (end_time_resource<end_time_reservation):
          template_values = {
            'resource_id': resource_id,
            'error': 'The resource is not available during the hours chosen'
          }
          template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
          self.response.write(template.render(template_values))
          return

        reservations = list(Reservation_Table.gql("WHERE resource_id = :1",resource_id))

        for reservation in reservations:
          if (start_time_reservation > reservation.start_time and start_time_reservation <reservation.end_time) or (end_time_reservation > reservation.start_time and end_time_reservation <reservation.end_time):
            template_values = {
            'resource_id': resource_id,
            'error': 'There is already a reservation at this time'
            }
            template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
            self.response.write(template.render(template_values))
            return
        
        user = users.get_current_user()
        current_time = time.strftime("%H:%M:%S")
        current_time2 = datetime.datetime.now()
        
        start_time_reservation = date + " " + start_time
        start_time_reservation = datetime.datetime.strptime(start_time_reservation, '%m/%d/%Y %H:%M')

        end_time_reservation = date + " " + end_time_string
        end_time_reservation = datetime.datetime.strptime(end_time_reservation, '%m/%d/%Y %H:%M')

        reservation = Reservation_Table(parent=reservation_key(current_time))
        reservation.reservation_id = str(uuid.uuid4())
        reservation.resource_id = resource_id
        reservation.username = user.nickname()
        reservation.start_time = start_time_reservation
        reservation.end_time = end_time_reservation
        reservation.resource_name = resource_name
        reservation.put()

        resource_update = list(Resource_Table.gql("WHERE resource_id = :1",resource_id))
        resource_update[0].last_modified = current_time2
        resource_update[0].put()


        self.redirect("/")
      
class DeleteReservation(webapp2.RequestHandler):
  def get(self):
        reservation_id = self.request.GET['reservation_id']
        reservation = list(Reservation_Table.gql("WHERE reservation_id = :1",reservation_id))
        reservation[0].key.delete()
        self.redirect("/")

class ViewUser(webapp2.RequestHandler):
  def get(self):
        username = self.request.GET['username']
        user_resources = list(Resource_Table.gql("WHERE username = :1",username))
        user_reservations = list(Reservation_Table.gql("WHERE username = :1",username))

        current_time = datetime.datetime.now()
        time_diff = datetime.datetime.strptime('05:00', '%H:%M')
        delta = datetime.timedelta(hours = 5, minutes = 0)
        current_time = current_time - delta

        template_values = {
          'user_resources': user_resources,
          'user_reservations': user_reservations,
          'current_time': current_time,
        }      
        template = JINJA_ENVIRONMENT.get_template('view_user.html')
        self.response.write(template.render(template_values))

class Tag(webapp2.RequestHandler):
  def get(self):
        tagged_resources = list()
        tag_name = self.request.GET['tag_name']
        resources = list(Resource_Table.gql(""))
        for resource in resources:
          tags = resource.tags
          for tag in tags:
            if tag == tag_name:
              tagged_resources.append(resource)

        template_values = {
        'tagged_resources': tagged_resources
        }
        template = JINJA_ENVIRONMENT.get_template('tagged_resources.html')
        self.response.write(template.render(template_values))
        return

class EditResource(webapp2.RequestHandler):
  def post(self):
        resource_id = str(self.request.get('resource_id'))
        resource = list(Resource_Table.gql("WHERE resource_id = :1",resource_id))
        
        tag_string = ""
        tags = list(resource[0].tags)
        for tag in tags:
          tag_string = tag_string + tag + ","

        #tags.split()
        template_values = {
        'resource': resource,
        'tag_string': tag_string
        }
        template = JINJA_ENVIRONMENT.get_template('edit_resource.html')
        self.response.write(template.render(template_values))

class EditResourceData(webapp2.RequestHandler):
  def post(self):
        isError = 0  
        resource_id = str(self.request.get('resource_id'))
        resource = list(Resource_Table.gql("WHERE resource_id = :1",resource_id))
        tag_string = ""
        tags = list(resource[0].tags)
        for tag in tags:
          tag_string = tag_string + tag + ","
        
        new_resource_name = self.request.get('resource_name')
        new_start_time = self.request.get('resource_start_time')
        new_end_time = self.request.get('resource_end_time')
        new_tags = self.request.get('tags')

        if len(new_resource_name) == 0:
          isError = 1
          template_values = {
              'resource': resource,
              'error': 'Resource name cannot be empty',
              'tag_string': tag_string
          }
          template = JINJA_ENVIRONMENT.get_template('edit_resource.html')
          self.response.write(template.render(template_values))
          return
        elif new_start_time > new_end_time:
          template_values = {
              'resource': resource,
              'error': 'Start time should be before end time',
              'tag_string': tag_string
            }
          template = JINJA_ENVIRONMENT.get_template('edit_resource.html')
          self.response.write(template.render(template_values))
          return

        elif isError == 0:
          try:
            time.strptime(new_start_time, '%H:%M')
          except ValueError:
            isError = 1
            template_values = {
              'resource': resource,
              'error': 'Start time format is incorrect and/or hours, minutes values incorrect ',
              'tag_string': tag_string
            }
            template = JINJA_ENVIRONMENT.get_template('edit_resource.html')
            self.response.write(template.render(template_values))
            return

        try:
          time.strptime(new_end_time, '%H:%M')
          template_values = dict()
          
          resource = list(Resource_Table.gql("WHERE resource_id = :1",resource_id))
          resource[0].resource_name = new_resource_name
          resource[0].start_time = datetime.datetime.strptime(new_start_time, '%H:%M')
          resource[0].end_time = datetime.datetime.strptime(new_end_time, '%H:%M')
          resource[0].tags = new_tags.strip().split(',')
          resource[0].put()    
          self.redirect("/")

        except ValueError:
          isError = 1
          template_values = {
            'resource': resource,
            'error': 'End time format is incorrect and/or hours, minutes values incorrect ',
            'tag_string': tag_string
          }
          template = JINJA_ENVIRONMENT.get_template('edit_resource.html')
          self.response.write(template.render(template_values))        

class CreateRSS(webapp2.RequestHandler):
  def get(self):
        resource_id = self.request.GET['resource_id']
        resource = list(Resource_Table.gql("WHERE resource_id = :1",resource_id))
        reservations = list(Reservation_Table.gql("WHERE resource_id = :1",resource_id))
        template_values = {
          'resource': resource,
          'reservations': reservations
        }
        template = JINJA_ENVIRONMENT.get_template('rss.html')
        self.response.write(template.render(template_values))        



app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/resource', Resource),
    ('/view_resource',ViewResource),
    ('/make_reservation', MakeReservation),
    ('/reservation', Reservation),
    ('/delete_reservation', DeleteReservation),
    ('/view_user', ViewUser),    
    ('/tag', Tag),
    ('/edit_resource', EditResource),
    ('/edit_resource_data', EditResourceData),
    ('/create_rss', CreateRSS)


], debug=True)