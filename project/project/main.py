#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import cgi
from google.appengine.api import users
from google.appengine.ext import ndb
import webapp2
import os
import urllib
from datetime import datetime
import jinja2
import time
import datetime
import uuid


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

def resource_key(current_time):
    return ndb.Key('Resource_Table', current_time)

class Resource_Table(ndb.Model):
    resource_id = ndb.StringProperty(indexed = True)
    resource_name = ndb.StringProperty(indexed = True)
    username = ndb.StringProperty(indexed = True)
    start_time = ndb.DateTimeProperty(indexed = False)
    end_time = ndb.DateTimeProperty(indexed = False)
    tags = ndb.StringProperty(indexed = False)

class MainPage(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user is None:
            self.redirect(users.create_login_url(self.request.uri))
        #q = GqlQuery("SELECT * FROM Resource_Table WHERE resource_name = 'rojdo'")
        q = Resource_Table.gql("")
        total_resources = list(q)
        q = Resource_Table.gql("WHERE username = :1",user.nickname())
        user_resources = list(q)
        template_values = {
            'user_resources' : user_resources,
            'total_resources' : total_resources
        }
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))

class Resource(webapp2.RequestHandler):
    def get(self):
        template_values = dict()

        template = JINJA_ENVIRONMENT.get_template('create_resource.html')
        self.response.write(template.render(template_values))

    def post(self):
        isError = 0
        resource_name = self.request.get('resource_name')
        start_time = self.request.get('resource_start_time')
        end_time = self.request.get('resource_end_time')
        tags = self.request.get('tags')

        if len(resource_name) == 0:
          isError = 1
          template_values = {
              'error': 'Resource Name cannot be empty',
          }
          template = JINJA_ENVIRONMENT.get_template('create_resource.html')
          self.response.write(template.render(template_values))
          return
        elif start_time > end_time:
          template_values = {
              'error': 'Start time should be before end time',
            }
          template = JINJA_ENVIRONMENT.get_template('create_resource.html')
          self.response.write(template.render(template_values))
          return

        elif isError == 0:
          try:
            time.strptime(start_time, '%H:%M')
          except ValueError:
            isError = 1
            template_values = {
              'error': 'Start time format is incorrect and/or hours, minutes values incorrect ',
            }
            template = JINJA_ENVIRONMENT.get_template('create_resource.html')
            self.response.write(template.render(template_values))
            return

        try:
          time.strptime(end_time, '%H:%M')
          template_values = dict()
          user = users.get_current_user()
          
          current_time = time.strftime("%H:%M:%S")
          resource = Resource_Table(parent=resource_key(current_time))
          resource.resource_id = str(uuid.uuid4())
          resource.resource_name = resource_name
          resource.username = user.nickname()
          resource.start_time = datetime.datetime.strptime(start_time, '%H:%M')
          resource.end_time = datetime.datetime.strptime(end_time, '%H:%M')
          resource.tags = tags

          resource.put()

          #template = JINJA_ENVIRONMENT.get_template('index.html')
          #self.redirect(template.render(template_values))
          self.redirect("/")

        except ValueError:
          isError = 1
          template_values = {
            'error': 'End time format is incorrect and/or hours, minutes values incorrect ',
          }
          template = JINJA_ENVIRONMENT.get_template('create_resource.html')
          self.response.write(template.render(template_values))

class Reservation(webapp2.RequestHandler):
  def get(self):
        resource = self.request.GET['resource']
        template_values = {
          'resource': resource
        }      
        template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
        self.response.write(template.render(template_values))

  def post(self):
        resource_id = self.request.get('resource_id')
        start_time = self.request.get('reservation_start_time')
        duration = self.request.get('duration')
        isError = 0
        '''
        template_values = {
          'resource': resource_id
        }

        template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
        self.response.write(template.render(template_values))'''
        try:
          time.strptime(start_time, '%H:%M')
        except ValueError:
          isError = 1
          template_values = {
            'error': 'Start time format is incorrect and/or hours, minutes values incorrect ',
            #'resource': resource
          }
          template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
          self.response.write(template.render(template_values))
          return

        try:
          time.strptime(duration, '%H:%M')
        except ValueError:
          isError = 1
          template_values = {
            'error': 'Duration format is incorrect and/or hours, minutes values incorrect ',
            #'resource': resource
          }
          template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
          self.response.write(template.render(template_values))
          return

        #test = list(Resource_Table.gql("WHERE resource_id = :1",resource_id))
        template_values = {
          'resource': resource_id
        }
        template = JINJA_ENVIRONMENT.get_template('make_reservation.html')
        self.response.write(template.render(template_values))




        

        
        
        
        

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/resource', Resource),
    ('/reservation', Reservation)


], debug=True)